# OpenML dataset: AfriSenti

https://www.openml.org/d/45282

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

We introduce AfriSenti, which consists of 14 sentiment datasets of 110,000+ tweets in 14 African languages (Amharic, Algerian Arabic, Hausa, Igbo, Kinyarwanda, Moroccan Arabic, Mozambican Portuguese, Nigerian Pidgin, Oromo, Swahili, Tigrinya, Twi, Xitsonga, and \yoruba) from four language families annotated by native speakers. The data was used in SemEval 2023 Task 12, the first Afro-centric SemEval shared task. We hope AfriSenti enables new work on under-represented languages. The dataset is available at https://github.com/afrisenti-semeval/afrisent-semeval-2023.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45282) of an [OpenML dataset](https://www.openml.org/d/45282). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45282/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45282/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45282/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

